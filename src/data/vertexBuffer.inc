// vertex buffer data to be included by render.hpp

#define VERTEX_BUFFER_SQUARE_OFFSET 0
#define VERTEX_BUFFER_SQUARE_LENGTH 6


#define VERTEX_BUFFER_DATA_SIZE 3 * (VERTEX_BUFFER_SQUARE_OFFSET + VERTEX_BUFFER_SQUARE_LENGTH)

#define VERTEX_BUFFER_DATA { \
\
/* --- 0 --- */ \
/* square [vertex pos - 3] */ \
  -0.5f, -0.5f,   0.0f\
  -0.5f,  0.5f, \
   0.5f, -0.5f, \
  -0.5f,  0.5f, \
   0.5f, -0.5f, \
   0.5f,  0.5f, \
}
  