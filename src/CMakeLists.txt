# minimum required cmake version: 3.18.0
cmake_minimum_required(VERSION 3.18.0)

# set include paths
set(MISSION_COMPUTER_INCLUDE ../submodules/mission-computer/src)
set(GLM_INCLUDE ../submodules/glm)

# find OpenGL
find_package(glfw3 3.3 REQUIRED)
find_package(OpenGL REQUIRED)

# find GLEW
find_library(NAMES GLEW PATHS libGLEW.a libGLEW.so.2.2)

# let the linker know we use /usr/lib64
link_directories(/usr/lib64)

set(CD_MODULE_SOURCES
    hackadon.cpp
    hackadon.hpp
    render.cpp
    render.hpp
)

project(hackadon ${USED_LANGUAGES})

# create executable target for the module
add_executable(${PROJECT_NAME} ${CD_MODULE_SOURCES})

# add include directory to mission-computer
target_include_directories(${PROJECT_NAME} PRIVATE ${MISSION_COMPUTER_INCLUDE} ${GLM_INCLUDE})

# copy OpenGL shaders and textures
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/shader ${CMAKE_BINARY_DIR}/. DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/shader)
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/textures ${CMAKE_BINARY_DIR}/. DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/textures)
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/maps ${CMAKE_BINARY_DIR}/. DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/maps)

# link external libraries to the target
target_link_libraries(${PROJECT_NAME} PUBLIC glfw3 OpenGL::GL GLEW pthread m dl)
