#pragma once

// includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <atomic>
#include <cmath>
#include <csignal>
#include <cstdlib>
#include <experimental/random>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/transform.hpp> 
#include <glm/gtx/quaternion.hpp>

#include "data/vertexBuffer.inc"

// defines
#define WINDOW_TITLE "Hackadon"
#define WINDOW_INITIAL_WIDTH 1024
#define WINDOW_INITIAL_HEIGHT 768
#define FIELD_OF_VIEW 60.0f
#define RENDER_DISTANCE 1000.0f
#define SPEED_MULT 0.1
#define ACCELERATION_MULT 0.2
#define ROLLOUT_MULT 0.1
#define MAX_SPEED 64.0
#define MIN_SPEED -16.0
#define TURN_MULT 0.15
#define TURN_SPEED 0.01
#define CAMERA_DISTANCE_MULT 2
#define FPS_UPDATE_RATE 1
#define SCROLL_FOV_MULT 3

#define TEXTURE_VARIANTS 8
#define INDEX_GND_ASPHALT_0 0
#define INDEX_GND_ASPHALT_1 INDEX_GND_ASPHALT_0 + TEXTURE_VARIANTS
#define INDEX_GND_COBBLESTONE_0 INDEX_GND_ASPHALT_1 + TEXTURE_VARIANTS
#define INDEX_GND_COBBLESTONE_1 INDEX_GND_COBBLESTONE_0 + TEXTURE_VARIANTS
#define INDEX_GND_COBBLESTONE_2 INDEX_GND_COBBLESTONE_1 + TEXTURE_VARIANTS
#define INDEX_GND_CONCRETE_0 INDEX_GND_COBBLESTONE_2 + TEXTURE_VARIANTS
#define INDEX_GND_FOOTPATH_0 INDEX_GND_CONCRETE_0 + TEXTURE_VARIANTS
#define INDEX_GND_FOOTPATH_1 INDEX_GND_FOOTPATH_0 + TEXTURE_VARIANTS
#define INDEX_GND_GRASS_0 INDEX_GND_FOOTPATH_1 + TEXTURE_VARIANTS
#define INDEX_GND_GRASS_1 INDEX_GND_GRASS_0 + TEXTURE_VARIANTS
#define INDEX_GND_GRASS_2 INDEX_GND_GRASS_1 + TEXTURE_VARIANTS
#define INDEX_GND_GRAVEL_0 INDEX_GND_GRASS_2 + TEXTURE_VARIANTS
#define INDEX_GND_GRAVEL_1 INDEX_GND_GRAVEL_0 + TEXTURE_VARIANTS
#define INDEX_GND_PEBBLES_0 INDEX_GND_GRAVEL_1 + TEXTURE_VARIANTS
#define INDEX_GND_RAILROAD_0 INDEX_GND_PEBBLES_0 + TEXTURE_VARIANTS
#define INDEX_GND_ROOF_0 INDEX_GND_RAILROAD_0 + TEXTURE_VARIANTS
#define INDEX_GND_ROOF_1 INDEX_GND_ROOF_0 + TEXTURE_VARIANTS
#define INDEX_GND_ROOF_2 INDEX_GND_ROOF_1 + TEXTURE_VARIANTS
#define INDEX_GND_ROOF_3 INDEX_GND_ROOF_2 + TEXTURE_VARIANTS
#define INDEX_GND_ROOF_4 INDEX_GND_ROOF_3 + TEXTURE_VARIANTS
#define INDEX_GND_WATER_0 INDEX_GND_ROOF_4 + TEXTURE_VARIANTS
#define INDEX_GND_WATER_1 INDEX_GND_WATER_0 + TEXTURE_VARIANTS
#define INDEX_LAST INDEX_GND_WATER_1 + TEXTURE_VARIANTS - 1


namespace render {

  // forward declaration of classes
  class RenderException;
  class Render;
  class Map;

  // generic exception class
  class RenderException: public std::exception {

  public:

    RenderException();
    RenderException(std::string s);

    const char *what() const throw();

  protected:
    std::string message;

  };

  // exception to be thrown on a OpenGL failures
  class RenderOpenGLException: public RenderException {

  public:

    RenderOpenGLException();
    RenderOpenGLException(std::string s);

  };

  // exception to be thrown on thread failures
  class RenderThreadException: public RenderException {

  public:

    RenderThreadException();
    RenderThreadException(std::string s);

  };

  class Render {

  public:
    Render();
    ~Render();

    void joinThreads();

    void incrementScrollOffset(int8_t offset);

    // friends
    friend void terminationSignal(int signum);

  protected:
    template<void (Render::*func)()>
    static void *worker(void *selfPtr);

    inline void threadInit();

    inline void renderInit();
    inline void inputInit();
    inline void frameInit();

    inline void setupGLEW();
    inline void setupGLFW();
    inline void setupGL();

    inline void buildBuffers();
    void buildTUI();


    GLuint loadShaders(std::string vertexShaderPath, std::string fragmentShaderPath);
    GLuint loadTexture(std::string texturePath);

    inline void renderLoop();
    inline void inputLoop();
    inline void frameLoop();

    void generateCamera(int width, int height);
    inline glm::mat4 generateProjection();
    inline glm::mat4 generateStaticProjection();

    inline void draw(uint8_t length, uint16_t offset, glm::mat4 *projection, GLuint matrixID, GLuint textureID);

    void resetView();

    // variable settings
    uint16_t scrollOffset = 1;
    bool debugMode = true;

    // threads
    static pthread_t mainThread;
    static pthread_t renderThread;
    static pthread_t inputThread;
    static pthread_t frameThread;

    // GLFW attributes
    GLFWwindow *window;
    int windowWidth;
    int windowHeight;

    // GL attributes
    GLuint program;
    GLuint vertexArray;

    GLuint vertexBuffer;
    GLuint colorBuffer;

    glm::vec3 viewPosition;
    glm::quat viewDirection;

    glm::mat4 camera;

    // fps counter
    uint16_t fps = 0;

    // buffer data
    GLfloat vertexBufferData[VERTEX_BUFFER_DATA_SIZE] = VERTEX_BUFFER_DATA;

    // performance measuring
    std::atomic<uint16_t> frameCounter = 0;

    // map data
    Map *map;

  };

  class Map {

  public:

    // forward declaration of public structs
    struct Tile; 

    Map(std::string pathBlock, std::string pathMaterial);
    ~Map();

    inline uint16_t getSizeX();
    inline uint16_t getSizeY();
    inline Tile *getTile(uint16_t x, uint16_t y);
    struct Tile {
      bool blocking;

      union Visual {
        enum class Material : uint16_t {
          ASPHALT_0 = INDEX_GND_ASPHALT_0,
          ASPHALT_1 = INDEX_GND_ASPHALT_1,
          COBBLESTONE_0 = INDEX_GND_COBBLESTONE_0,
          COBBLESTONE_1 = INDEX_GND_COBBLESTONE_1,
          COBBLESTONE_2 = INDEX_GND_COBBLESTONE_2,
          CONCRETE_0 = INDEX_GND_CONCRETE_0,
          FOOTPATH_0 = INDEX_GND_FOOTPATH_0,
          FOOTPATH_1 = INDEX_GND_FOOTPATH_1,
          GRASS_0 = INDEX_GND_GRASS_0,
          GRASS_1 = INDEX_GND_GRASS_1,
          GRASS_2 = INDEX_GND_GRASS_2,
          GRAVEL_0 = INDEX_GND_GRAVEL_0,
          GRAVEL_1 = INDEX_GND_GRAVEL_1,
          PEBBLES_0 = INDEX_GND_PEBBLES_0,
          RAILROAD_0 = INDEX_GND_RAILROAD_0,
          ROOF_0 = INDEX_GND_ROOF_0,
          ROOF_1 = INDEX_GND_ROOF_1,
          ROOF_2 = INDEX_GND_ROOF_2,
          ROOF_3 = INDEX_GND_ROOF_3,
          ROOF_4 = INDEX_GND_ROOF_4,
          WATER_0 = INDEX_GND_WATER_0,
          WATER_1 = INDEX_GND_WATER_1
        } material;
        struct {
          uint8_t variant : 4;
          uint16_t base : 12;
        } render;
        uint16_t index;
      } visual;
    };

  protected:

    inline void loadBlock(std::string blockPath);
    inline void loadMaterial(std::string materialPath);

    Tile* tiles;
    uint16_t sizeX = 0;
    uint16_t sizeY = 0;

  };

}
