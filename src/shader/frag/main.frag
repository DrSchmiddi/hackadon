#version 430 core

// inputs
in vec2 uv;

// outputs
out vec3 color;

// uniforms
uniform sampler2D textureSampler;


void main(){

  // set the color
  color = texture(textureSampler, uv).rgb;
}

