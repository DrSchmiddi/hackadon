#include "render.hpp"

namespace render {


  // declaration of static variables

  static Render* callbackObject;


  // definition of static members

  pthread_t Render::mainThread;
  pthread_t Render::renderThread;
  pthread_t Render::inputThread;
  pthread_t Render::frameThread;


  // forward declaration of static functions

  static inline glm::quat angleToQuat(double angle); 
  static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);


  // forward declaration of friend functions

  void terminationSignal(int signum);


  // definition of static functions

  static inline glm::quat angleToQuat(double angle) {

    return glm::quat(cos(angle), 0, 0, sin(angle));
  }

  static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {

    // we only care about the y-component
    (void) window;
    (void) xoffset;

    // store the current inverted offset
    callbackObject->incrementScrollOffset(static_cast<int8_t>(-yoffset));
  }


  // definition of friend functions

  void terminationSignal(int signum) {

    // we do not care about the signal number
    (void) signum;

    pthread_exit(nullptr);
  }


  // RenderException

  RenderException::RenderException() {
    message = "RenderException";
    
  }

  RenderException::RenderException(std::string s) {
    message = s;

  }

  const char *RenderException::what() const throw() {
    return message.c_str();
  }


  // RenderOpenGLException

  RenderOpenGLException::RenderOpenGLException() : RenderException("RenderOpenGLException") {}
  RenderOpenGLException::RenderOpenGLException(std::string s) : RenderException(s) {} 


  // RenderThreadException

  RenderThreadException::RenderThreadException() : RenderException("RenderThreadException") {}
  RenderThreadException::RenderThreadException(std::string s) : RenderException(s) {} 


  // Render

  Render::Render() {

    // store reference to the main thread
    mainThread = pthread_self();

    if (pthread_create(&renderThread, nullptr, worker<&Render::renderInit>, static_cast<void *>(this)) != 0) {
      throw new RenderThreadException("Failed to create render thread");
    }
  }

  Render::~Render() {

  }

  void Render::joinThreads() {

    // send custom termination signal to the threads to exit them peacefully
    pthread_kill(renderThread, SIGUSR1);
    pthread_kill(inputThread, SIGUSR1);
    pthread_kill(frameThread, SIGUSR1);

    // join with the render thread
    if (pthread_join(renderThread, nullptr) != 0) {
      throw new RenderThreadException("Failed to join render thread");
    }

    // join with the input thread
    if (pthread_join(inputThread, nullptr) != 0) {
      throw new RenderThreadException("Failed to join input thread");
    }

    // join with the curses thread
    if (pthread_join(frameThread, nullptr) != 0) {
      throw new RenderThreadException("Failed to join curses thread");
    }

  }

  void Render::incrementScrollOffset(int8_t offset) {

    // do not allow overflows and underflows
    if (!((scrollOffset == 0 && 0 > offset) || (scrollOffset == static_cast<uint16_t>(-1) && 0 < offset))) {
      
      // increment the scroll offset
      scrollOffset += offset;
    }
  }

  template<void (Render::*func)()>
  void *Render::worker(void *selfPtr) {

    // cast the pointer into a Render object
    Render *self = static_cast<Render *>(selfPtr);

    // first initialize the thread
    self->threadInit();

    try {
      
      // call the init function so we are back in a non-static context
      (self->*func)();
    } catch (render::RenderOpenGLException* e) {
      std::cout << e->what() << std::endl;

      exit(errno);
    }

    // return trivial nullpointer
    return nullptr;
  }

  inline void Render::threadInit() {

    // initialize the callback function on SIGTERM
    signal(SIGUSR1, terminationSignal);
  }

  inline void Render::renderInit() {

    // initialize libraries
    setupGLFW();
    setupGLEW();
    setupGL();

    // build the buffers and vertex arrays
    buildBuffers();

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

    // load the shaders
    program = loadShaders("shader/vert/main.vert", "shader/frag/main.frag");

    map = new Map("maps/block.bmp", "maps/material.bmp");

    if (pthread_create(&inputThread, nullptr, worker<&Render::inputInit>, static_cast<void *>(this)) != 0) {
      throw new RenderThreadException("Failed to create input thread");
    }

    // start the render loop
    renderLoop();

  }

  inline void Render::inputInit() {

    callbackObject = this;

    if (pthread_create(&frameThread, nullptr, worker<&Render::frameInit>, static_cast<void *>(this)) != 0) {
      throw new RenderThreadException("Failed to create frame thread");
    }

    // start the input loop
    inputLoop();

  }

  inline void Render::frameInit() {

    // start the frame loop
    frameLoop();

  }

  inline void Render::setupGLEW() {

    // enable experimental mode
    glewExperimental = true;

    if (glewInit() != GLEW_OK) {

      // exit if we could not initialize GLEW
      throw new RenderOpenGLException("Failed to initialize GLEW");
    }

  }

  inline void Render::setupGLFW() {

    // initialise GLFW
    if(!glfwInit()) {
      throw new RenderOpenGLException("Failed to initialize GLFW");
    }

    // 4X antialiasing
    glfwWindowHint(GLFW_SAMPLES, 4);

    // OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);

    // legacy code
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // open a window and create its OpenGL context
    window = glfwCreateWindow(WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, WINDOW_TITLE, nullptr, nullptr);

    if (window == nullptr){

      // exit if we could not open the window
      glfwTerminate();
      throw new RenderOpenGLException("Failed to open GLFW window");
    }

    // set the GLFW window
    glfwMakeContextCurrent(window);
  }

  inline void Render::setupGL() {

    // disable depth test
    glDisable(GL_DEPTH_TEST);

    // draw textures
    glPolygonMode(GL_FRONT, GL_BITMAP);

    // generate camera matrix and view port
    generateCamera(WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT);

    // set to the initial window dimensions
    windowWidth = WINDOW_INITIAL_WIDTH;
    windowHeight = WINDOW_INITIAL_HEIGHT;

    // initialize view position
    resetView();

  }

  inline void Render::buildBuffers() {

    // generate and bind the vertex array
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);

    // generate and bind the vertex buffer
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    
  }

  GLuint Render::loadShaders(std::string vertexShaderPath, std::string fragmentShaderPath) {

    // create the shaders
    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // read the vertex shader code from the file
    std::string vertexShaderCode;
    std::ifstream vertexShaderStream(vertexShaderPath, std::ios::in);

    if (vertexShaderStream.is_open()){
      std::stringstream sstr;
      sstr << vertexShaderStream.rdbuf();
      vertexShaderCode = sstr.str();
      vertexShaderStream.close();
    } else {

      // throw an error if we could not open the file
      throw new RenderOpenGLException("Failed to open vertex shader: " + vertexShaderPath);
    }

    // read the fragment shader code from the file
    std::string fragmentShaderCode;
    std::ifstream fragmentShaderStream(fragmentShaderPath, std::ios::in);

    if (fragmentShaderStream.is_open()){
      std::stringstream sstr;
      sstr << fragmentShaderStream.rdbuf();
      fragmentShaderCode = sstr.str();
      fragmentShaderStream.close();
    } else {

      // throw an error if we could not open the file
      throw new RenderOpenGLException("Failed to open fragment shader: " + fragmentShaderPath);
    }

    GLint res = GL_FALSE;
    int infoLogLength;

    // compile vertex shader
    char const * vertexSourcePointer = vertexShaderCode.c_str();
    glShaderSource(vertexShaderID, 1, &vertexSourcePointer , NULL);
    glCompileShader(vertexShaderID);

    // check vertex shader
    glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &res);
    glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (infoLogLength > 0){
      std::vector<char> vertexShaderErrorMessage(infoLogLength+1);
      glGetShaderInfoLog(vertexShaderID, infoLogLength, NULL, &vertexShaderErrorMessage[0]);

      throw new RenderOpenGLException("Failed to compile vertex shader: " + std::string(&vertexShaderErrorMessage[0]));
    }

    // compile fragment shader
    char const * fragmentSourcePointer = fragmentShaderCode.c_str();
    glShaderSource(fragmentShaderID, 1, &fragmentSourcePointer , NULL);
    glCompileShader(fragmentShaderID);

    // check fragment shader
    glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &res);
    glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (infoLogLength > 0){
      std::vector<char> fragmentShaderErrorMessage(infoLogLength+1);
      glGetShaderInfoLog(fragmentShaderID, infoLogLength, NULL, &fragmentShaderErrorMessage[0]);

      throw new RenderOpenGLException("Failed to compile fragment shader: " + std::string(&fragmentShaderErrorMessage[0]));
    }

    // create the program
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShaderID);
    glAttachShader(program, fragmentShaderID);
    glLinkProgram(program);

    // check the program
    glGetProgramiv(program, GL_LINK_STATUS, &res);
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (infoLogLength > 0){
      std::vector<char> programErrorMessage(infoLogLength+1);
      glGetProgramInfoLog(program, infoLogLength, NULL, &programErrorMessage[0]);

      throw new RenderOpenGLException("Program error");
    }
    
    glDetachShader(program, vertexShaderID);
    glDetachShader(program, fragmentShaderID);
    
    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);

    // return the generated program
    return program;
  }

  GLuint Render::loadTexture(std::string texturePath) {

    const uint8_t standardSize = 54;

    // data read from the header of the bmp file
    uint8_t header[standardSize];
    
    // ppen the file
    FILE * file = fopen(texturePath.c_str(), "rb");

    if (!file) {

      throw new RenderOpenGLException("Failed to open texture: " + texturePath);
    }

    if (fread(header, 1, standardSize, file) != standardSize) {

      throw new RenderOpenGLException("Not a correct texture");
    }

    if (header[0] != 'B' || header[1] != 'M'){

      throw new RenderOpenGLException("Not a correct texture");
    }

    // readfrom the byte array
    uint32_t dataPos = *(uint32_t *) &header[0x0A];
    const uint32_t offset = dataPos - standardSize;

    uint32_t imageSize = (*(uint32_t *) &header[0x22]) + offset;
    const uint32_t width = *(uint32_t *) &header[0x12];
    const uint32_t height = *(uint32_t *) &header[0x16];

    // guess missing information
    if (imageSize == offset) {

      imageSize += width * height * 3;
    }

    if (dataPos == 0) {

      // skip the header
      dataPos = 54;
    }

    // create a buffer
    uint8_t *data = new uint8_t[imageSize];
    uint8_t *dataOffset = data + offset;

    // read data from the file into the buffer
    size_t size = fread(data, 1, imageSize, file);
    (void) size;

    fclose(file);

    // create texture
    GLuint ret;
    glGenTextures(1, &ret);

    // bind the newly created texture
    glBindTexture(GL_TEXTURE_2D, ret);

    // give the image to OpenGL
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, dataOffset);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    return ret;
  }

  inline void Render::renderLoop() {

    GLuint groundTextures[INDEX_LAST];
    GLuint busTexture;
    GLuint matrixID;

    // local configuring
    {

      // texture loading lambda
      auto loadTextures = [this, &groundTextures](uint16_t index, std::string path) {

        for (uint8_t i = 0; i < TEXTURE_VARIANTS; i++) {
          groundTextures[index + i] = loadTexture(path + "_" + std::to_string(i) + ".bmp");
        }
      };

      // load all textures
      loadTextures(INDEX_GND_ASPHALT_0, "textures/gnd_asphalt_0");
      loadTextures(INDEX_GND_ASPHALT_1, "textures/gnd_asphalt_1");
      loadTextures(INDEX_GND_COBBLESTONE_0, "textures/gnd_cobblestone_0");
      loadTextures(INDEX_GND_COBBLESTONE_1, "textures/gnd_cobblestone_1");
      loadTextures(INDEX_GND_COBBLESTONE_2, "textures/gnd_cobblestone_2");
      loadTextures(INDEX_GND_CONCRETE_0, "textures/gnd_concrete_0");
      loadTextures(INDEX_GND_FOOTPATH_0, "textures/gnd_footpath_0");
      loadTextures(INDEX_GND_FOOTPATH_1, "textures/gnd_footpath_1");
      loadTextures(INDEX_GND_GRASS_0, "textures/gnd_grass_0");
      loadTextures(INDEX_GND_GRASS_1, "textures/gnd_grass_1");
      loadTextures(INDEX_GND_GRASS_2, "textures/gnd_grass_2");
      loadTextures(INDEX_GND_GRAVEL_0, "textures/gnd_gravel_0");
      loadTextures(INDEX_GND_GRAVEL_1, "textures/gnd_gravel_1");
      loadTextures(INDEX_GND_PEBBLES_0, "textures/gnd_pebbles_0");
      loadTextures(INDEX_GND_RAILROAD_0, "textures/gnd_railroad_0");
      loadTextures(INDEX_GND_ROOF_0, "textures/gnd_roof_0");
      loadTextures(INDEX_GND_ROOF_1, "textures/gnd_roof_1");
      loadTextures(INDEX_GND_ROOF_2, "textures/gnd_roof_2");
      loadTextures(INDEX_GND_ROOF_3, "textures/gnd_roof_3");
      loadTextures(INDEX_GND_ROOF_4, "textures/gnd_roof_4");
      loadTextures(INDEX_GND_WATER_0, "textures/gnd_water_0");
      loadTextures(INDEX_GND_WATER_1, "textures/gnd_water_1");

      // load the bus texture
      busTexture = loadTexture("textures/new/obj_bus_0_0.bmp");

      // get a GL matrix ID
      matrixID = glGetUniformLocation(program, "projection");
    }

    uint16_t ind = 0;

    while (true) {

      ind++;

      int currentWindowWidth, currentWindowHeight;

      // get the window dimensions
      glfwGetWindowSize(window, &currentWindowWidth, &currentWindowHeight);

      if ((currentWindowWidth != windowWidth) || (currentWindowHeight != windowHeight)) {

        // regenerate camera matrix and view port
        generateCamera(currentWindowWidth, currentWindowHeight);

        // update window dimensions variables
        windowWidth = currentWindowWidth;
        windowHeight = currentWindowHeight;
      }

      // calculate the global projection matrices
      glm::mat4 globalProjection = generateProjection();
      glm::mat4 globalStaticProjection = generateStaticProjection();

      // clear the screen
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // use the program with the shaders loaded
      glUseProgram(program);

      // enable the vertex arrays
      glEnableVertexAttribArray(0);
       
      // render ground plane
      {

        static const uint16_t sizeX = map->getSizeX();
        static const uint16_t sizeY = map->getSizeY();
        static const uint16_t sizeX_2 = sizeX / 2;
        static const uint16_t sizeY_2 = sizeY / 2;

        // define render area
        int16_t xStart = sizeX_2 - viewPosition.x - SCROLL_FOV_MULT * scrollOffset - 1;
        int16_t xEnd = sizeX_2 - viewPosition.x + SCROLL_FOV_MULT * scrollOffset + 1;
        int16_t yStart = sizeY_2 - viewPosition.y - SCROLL_FOV_MULT * scrollOffset - 1;
        int16_t yEnd = sizeY_2 - viewPosition.y + SCROLL_FOV_MULT * scrollOffset + 1;

        if (xStart < 0) {
          xStart = 0;
        }

        if (xEnd >= sizeX) {
          xEnd = sizeX - 1;
        }

        if (yStart < 0) {
          yStart = 0;
        }

        if (yEnd >= sizeY) {
          yEnd = sizeY - 1;
        }

        for (uint16_t i = xStart; i < xEnd; i++) {

          for (uint16_t j = yStart; j < yEnd; j++) {

            // texture loading lambda
            auto tileToTextureIndex = [this, &groundTextures](Map::Tile &tile) {

              return groundTextures[tile.visual.index];
            };

            // calculate matrix
            glm::mat4 translation = glm::translate(glm::vec3(i - sizeX_2, j - sizeY_2, 0));
            glm::mat4 localProjection = globalProjection * translation;

            // draw a line in the ground plane
            draw(VERTEX_BUFFER_SQUARE_LENGTH, VERTEX_BUFFER_SQUARE_OFFSET, &localProjection, matrixID, tileToTextureIndex(*map->getTile(i, j)));
          }
        }
      }

      // render vehicle
      {

        // calculate matrix
        glm::mat4 scale = glm::scale(glm::vec3(2, 8, 1));
        glm::mat4 localProjection = globalStaticProjection * scale;

        // draw a line in the ground plane
        draw(VERTEX_BUFFER_SQUARE_LENGTH, VERTEX_BUFFER_SQUARE_OFFSET, &localProjection, matrixID, busTexture);

      }

      // disable the vertex arrays
      glDisableVertexAttribArray(0);

      // swap buffers
      glfwSwapBuffers(window);
      glfwPollEvents();

      // check if the the window was closed
      if (glfwWindowShouldClose(window) != 0) {

        sigset_t mask, oldmask;

        // setup the signal mask
        sigemptyset(&mask);
        sigaddset(&mask, SIGUSR1);

        // send termination signal to the main thread
        pthread_kill(mainThread, SIGINT);

        // wait for a signal to arrive
        {
          sigprocmask(SIG_BLOCK, &mask, &oldmask);

          while (true) {
            sigsuspend(&oldmask);
          }

          sigprocmask(SIG_UNBLOCK, &mask, NULL);
        }
      }

      // increment the frame counter
      frameCounter++;

    }

  }

  void Render::inputLoop() {

    // current, last and differential mouse position
    struct {
      double x;
      double y;
    } mouseCurrent, mouseLast;

    // initialize the last mouse position
    glfwGetCursorPos(window, &mouseLast.x, &mouseLast.y);

    // set the callback function for scrolling
    glfwSetScrollCallback(window, scrollCallback);

    // initialize the last key state of the toggle keys
    int debugModeKeyLast = GLFW_RELEASE;

    while (true) {

      // store the current mouse position
      glfwGetCursorPos(window, &mouseCurrent.x, &mouseCurrent.y);

      // position movement
      {

        static double direction = 0.0;
        static double speed = 0.0;

        if (speed != 0.0) {

          // decide on the direction based on key input
          if ((glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)) {

            // turn right
            direction += TURN_SPEED / (TURN_MULT * speed + 1.0);
          } else if ((glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)) {

            // turn left
            direction -= TURN_SPEED / (TURN_MULT * speed + 1.0);
          }
        }
        
        if ((glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)) {

          // deccelerate
          if (speed > 0) {
            speed -= ACCELERATION_MULT * sqrt(MAX_SPEED - speed + 1.0);
          } else {
            speed -= ACCELERATION_MULT * sqrt(-speed + 1.0);
          }

          if (speed < MIN_SPEED) {
            speed = MIN_SPEED;
          }
        } else if ((glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)) {

          // accelerate
          if (speed > 0) {
            speed += ACCELERATION_MULT * sqrt(speed + 1.0);
          } else {
            speed += ACCELERATION_MULT * sqrt(-MIN_SPEED + speed + 1.0);
          }

          if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
          }
        } else {

          // deccelerate without braking

          if (speed >= 0) {
            speed -= ROLLOUT_MULT * sqrt(speed);
          } else {
            speed += ROLLOUT_MULT * sqrt(-speed);
          }
        }

        glm::vec3 lastPosition = viewPosition;

        viewDirection = angleToQuat(direction);

        // calculate the world position from the view direction and position
        viewPosition += glm::toMat3(angleToQuat(-direction - M_PI_4)) * glm::vec3(speed * SPEED_MULT / MAX_SPEED, 0, 0);

        static const uint16_t sizeX_2 = map->getSizeX() / 2;
        static const uint16_t sizeY_2 = map->getSizeY() / 2;

        auto rotateRelative = [](float x, float y) {

          float localAngle = atan(x / y);
          float distance = sqrt(x * x + y * y);

          struct {
            float x;
            float y;
          } ret;


          if (y < 0) {
            ret.x = sin(-direction + localAngle + M_PI) * distance;
            ret.y = cos(-direction + localAngle + M_PI) * distance;
          } else {
            ret.x = sin(-direction + localAngle) * distance;
            ret.y = cos(-direction + localAngle) * distance;
          }

          return ret;

        };

        if (map->getTile(sizeX_2 - viewPosition.x + rotateRelative(1, 4).x, sizeY_2 - viewPosition.y + rotateRelative(1, 4).y)->blocking) {
          
          // reset on collision
          viewPosition = lastPosition;
        } else if (map->getTile(sizeX_2 - viewPosition.x + rotateRelative(1, -4).x, sizeY_2 - viewPosition.y + rotateRelative(1, -4).y)->blocking) {
          
          // reset on collision
          viewPosition = lastPosition;
        } else if (map->getTile(sizeX_2 - viewPosition.x + rotateRelative(-1, 4).x, sizeY_2 - viewPosition.y + rotateRelative(-1, 4).y)->blocking) {
          
          // reset on collision
          viewPosition = lastPosition;
        } else if (map->getTile(sizeX_2 - viewPosition.x + rotateRelative(-1, -4).x, sizeY_2 - viewPosition.y + rotateRelative(-1, -4).y)->blocking) {
          
          // reset on collision
          viewPosition = lastPosition;
        }
      }

      // shortcuts
      {

        // decide on the shortcut being executed
        if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {

          // reset the view position
          resetView();
        }

        // debug mode toggle
        {

          int tmp = glfwGetKey(window, GLFW_KEY_P);

          // only toggle on a key change
          if (tmp != debugModeKeyLast) {

            // only toggle on press
            if (tmp == GLFW_PRESS) {

              // toggle the debug mode flag
              debugMode = !debugMode;
            }

            // store the current state
            debugModeKeyLast = tmp;
          }
        }

      }

      // store the current mouse position for the next iteration
      mouseLast = mouseCurrent;

      // put the thread to sleep
      usleep(10000);

    }

  }

  void Render::frameLoop() {

    while (true) {

      // store the frame count and reset it
      uint16_t frames = frameCounter;
      frameCounter = 0;

      fps = frames * FPS_UPDATE_RATE;

      std::cout << fps << std::endl << std::flush;

      // put the thread to sleep
      usleep(1000000 / FPS_UPDATE_RATE);

    }

  }

  void Render::generateCamera(int width, int height) {

    // set the view port
    glViewport(0, 0, width, height);

    // regenerate camera matrix (left, right, bottom, top)
    camera = glm::perspective(glm::radians(FIELD_OF_VIEW), (float) width / (float) height, 0.1f, RENDER_DISTANCE);
    //camera = glm::ortho(0, width, height, 0);
  }

  inline glm::mat4 Render::generateProjection() {

    // generate matrix components
    const glm::mat4 translation = glm::translate(glm::mat4(1), viewPosition);
    const glm::mat4 rotation = glm::toMat4(viewDirection);

    // generate view matrix
    glm::mat4 view = rotation * translation;

    // apply scroll offset
    view = glm::translate(glm::vec3(0, 0, -CAMERA_DISTANCE_MULT * scrollOffset)) * view;

    // calculate the projection matrix
    return camera * view;

  }

  inline glm::mat4 Render::generateStaticProjection() {

    // apply scroll offset
    glm::mat4 view = glm::translate(glm::vec3(0, 0, -CAMERA_DISTANCE_MULT * scrollOffset));

    // calculate the projection matrix
    return camera * view;

  }

  inline void Render::draw(uint8_t length, uint16_t offset, glm::mat4 *projection, GLuint matrixID, GLuint textureID) {

    // set attributes pointers (include stride and byte offset as we store vertex and color data in the same buffer)
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void *) (sizeof(float) * 2 * offset));

    // send the matrix to the shaders
    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &(*projection)[0][0]);

    // send the texture to the shaders
    glActiveTexture(GL_TEXTURE0 + textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniform1i(1, textureID);

    // draw the verteces
    glDrawArrays(GL_TRIANGLES, 0, length);
  }

  void Render::resetView() {

    // reset the view position and direction
    viewPosition = glm::vec3(0);
    viewDirection = glm::quat(1, 0, 0, 0);
  }


  // Map

  Map::Map(std::string pathBlock, std::string pathMaterial) {

    // load data
    loadBlock(pathBlock);
    loadMaterial(pathMaterial);
  }

  Map::~Map() {

    delete[] tiles;
  }

  inline uint16_t Map::getSizeX() {

    return sizeX;
  }

  inline uint16_t Map::getSizeY() {

    return sizeY;
  }

  inline Map::Tile *Map::getTile(uint16_t x, uint16_t y) {

    return tiles + ((sizeY * x) + y);
  }

  inline void Map::loadBlock(std::string blockPath) {

    const uint8_t standardSize = 54;

    // data read from the header of the bmp file
    uint8_t header[standardSize];
    
    // ppen the file
    FILE * file = fopen(blockPath.c_str(), "rb");

    if (!file) {

      throw new RenderOpenGLException("Failed to open block map: " + blockPath);
    }

    if (fread(header, 1, standardSize, file) != standardSize) {

      throw new RenderOpenGLException("Not a correct block map");
    }

    if (header[0] != 'B' || header[1] != 'M'){

      throw new RenderOpenGLException("Not a correct block map");
    }

    // readfrom the byte array
    uint32_t dataPos = *(uint32_t *) &header[0x0A];
    const uint32_t offset = dataPos - standardSize;

    uint32_t imageSize = (*(uint32_t *) &header[0x22]) + offset;
    const uint32_t width = *(uint32_t *) &header[0x12];
    const uint32_t height = *(uint32_t *) &header[0x16];

    // guess missing information
    if (imageSize == offset) {

      imageSize += width * height * 3;
    }

    if (dataPos == 0) {

      // skip the header
      dataPos = 54;
    }

    // create a buffer
    uint8_t *data = new uint8_t[imageSize];
    uint8_t *dataOffset = data + offset;

    // read data from the file into the buffer
    size_t size = fread(data, 1, imageSize, file);
    (void) size;

    fclose(file);

    if (sizeX != 0) {

      if (sizeX != width) {
        throw new RenderException("Different block and material dimensions.");
      }
    } else {

      if (sizeY != 0) {

        if (sizeY != height) {
          throw new RenderException("Different block and material dimensions.");
        }
      } else {

        // set initial values
        sizeX = width;
        sizeY = height;

        // create the tile field
        tiles = new Tile[sizeX * sizeY];
      }
    }

    for (uint16_t i = 0; i < sizeX; i++) {

      for (uint16_t j = 0; j < sizeY - 1; j++) {
      
        getTile(i, j)->blocking = dataOffset[3 * (((sizeX + 1) * j) + i)] != 0;
      }
    }

    delete data;
  }

  inline void Map::loadMaterial(std::string materialPath) {

    const uint8_t standardSize = 54;

    // data read from the header of the bmp file
    uint8_t header[standardSize];
    
    // ppen the file
    FILE * file = fopen(materialPath.c_str(), "rb");

    if (!file) {

      throw new RenderOpenGLException("Failed to open block map: " + materialPath);
    }

    if (fread(header, 1, standardSize, file) != standardSize) {

      throw new RenderOpenGLException("Not a correct block map");
    }

    if (header[0] != 'B' || header[1] != 'M'){

      throw new RenderOpenGLException("Not a correct block map");
    }

    // readfrom the byte array
    uint32_t dataPos = *(uint32_t *) &header[0x0A];
    const uint32_t offset = dataPos - standardSize;

    uint32_t imageSize = (*(uint32_t *) &header[0x22]) + offset;
    const uint32_t width = *(uint32_t *) &header[0x12];
    const uint32_t height = *(uint32_t *) &header[0x16];

    // guess missing information
    if (imageSize == offset) {

      imageSize += width * height * 3;
    }

    if (dataPos == 0) {

      // skip the header
      dataPos = 54;
    }

    // create a buffer
    uint8_t *data = new uint8_t[imageSize];
    uint8_t *dataOffset = data + offset;

    // read data from the file into the buffer
    size_t size = fread(data, 1, imageSize, file);
    (void) size;
    
    fclose(file);

    if (sizeX != 0) {

      if (sizeX != width) {
        throw new RenderException("Different block and material dimensions.");
      }
    } else {

      if (sizeY != 0) {

        if (sizeY != height) {
          throw new RenderException("Different block and material dimensions.");
        }
      } else {

        // set initial values
        sizeX = width;
        sizeY = height;

        // create the tile map
        tiles = new Tile[sizeX * sizeY];
      }
    }

    for (uint16_t i = 0; i < sizeX; i++) {

      for (uint16_t j = 0; j < sizeY - 1; j++) {

        union DataPointer {
          struct RGB {
            uint32_t combined : 24;
          } *rgb;
          uint8_t *raw;
        } dataPointer;

        dataPointer.raw = dataOffset + 3 * (((sizeX + 1) * j) + i);

        Tile *tile = getTile(i, j);
      
        switch (dataPointer.rgb->combined) {

          case 0x000000: {

            tile->visual.material = Tile::Visual::Material::ASPHALT_0;
          } break;

          case 0x373737: {

            tile->visual.material = Tile::Visual::Material::ASPHALT_1;
          } break;

          case 0x004080: {

            tile->visual.material = Tile::Visual::Material::COBBLESTONE_0;
          } break;

          case 0x80ff80: {

            tile->visual.material = Tile::Visual::Material::COBBLESTONE_1;
          } break;

          case 0x80ffff: {

            tile->visual.material = Tile::Visual::Material::COBBLESTONE_2;
          } break;

          case 0xc2292e: {

            tile->visual.material = Tile::Visual::Material::CONCRETE_0;
          } break;

          case 0xb97a57: {

            tile->visual.material = Tile::Visual::Material::FOOTPATH_0;
          } break;

          case 0xdc101b: {

            tile->visual.material = Tile::Visual::Material::FOOTPATH_1;
          } break;

          case 0xfff0ff: {

            tile->visual.material = Tile::Visual::Material::GRASS_0;
          } break;

          case 0x00ff00: {

            tile->visual.material = Tile::Visual::Material::GRASS_1;
          } break;

          case 0x80ff00: {

            tile->visual.material = Tile::Visual::Material::GRASS_2;
          } break;

          case 0x7f7f7f: {

            tile->visual.material = Tile::Visual::Material::GRAVEL_0;
          } break;

          case 0xc3c3c3: {

            tile->visual.material = Tile::Visual::Material::GRAVEL_1;
          } break;

          case 0xefe4b0: {

            tile->visual.material = Tile::Visual::Material::PEBBLES_0;
          } break;

          case 0x7a101b: {

            tile->visual.material = Tile::Visual::Material::ROOF_0;
          } break;

          case 0xb3396a: {

            tile->visual.material = Tile::Visual::Material::ROOF_1;
          } break;

          case 0x8e5e63: {

            tile->visual.material = Tile::Visual::Material::ROOF_2;
          } break;

          case 0x22b14c: {

            tile->visual.material = Tile::Visual::Material::ROOF_3;
          } break;

          case 0x995355: {

            tile->visual.material = Tile::Visual::Material::ROOF_4;
          } break;

          case 0x2b44f4: {

            tile->visual.material = Tile::Visual::Material::WATER_1;
          } break;

          case 0x00a2e8: {

            tile->visual.material = Tile::Visual::Material::WATER_1;
          } break;

          case 0xffffff: {

            tile->visual.material = Tile::Visual::Material::GRASS_0;
          } break;

          default: {

            throw new RenderException("Unsupported material.");
          } break;

        }

        // define constant variant
        tile->visual.render.variant = std::experimental::randint(0, TEXTURE_VARIANTS - 1);
      }
    }

    delete data;
  }

}
