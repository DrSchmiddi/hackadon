# hackadon 2020

A fun little Hackadon project.

## install the libraries

1. fetch all submodules

```bash
git submodule init
git submodule update --recursive
git submodule foreach git pull origin master
```


2. run the following commands in the `submodules/glew` directory

```bash
make extensions
sudo make install
```

3. run the following command in the `submodules/glfw` directory

```bash
cmake .
sudo make install
```

4. add the following line to your `~/.bashrc`

```bash
export LD_LIBRARY_PATH=/usr/lib64
```

reload your shell config

```bash
source ~/.bashrc
```

## building
run in the project root directory to create build directory
```bash
mkdir build
cd build
```

run once in the `build/` directory
```bash
cmake ..
```

then everytime you need to build the project run:
```bash
make hackadon
```

## notes

1. textures need to be 24-bit bmp files
2. cmake version 3.18 is required
