# minimum required cmake version: 3.18.0
cmake_minimum_required(VERSION 3.18.0)

option(CD_BUILD_MODULES "build modules seperatly" OFF)

# set build types
set(CMAKE_CONFIGURATION_TYPES Debug Release)

if(NOT DEFINED CMAKE_BUILD_TYPE)

  set(CMAKE_BUILD_TYPE Debug)

endif()

# Enable C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

# set used languages
set(USED_LANGUAGES LANGUAGES CXX)

# set compiler flags
if(CMAKE_BUILD_TYPE MATCHES Release)

  set(CMAKE_CXX_FLAGS "-Werror -Wall -Wextra -Wpedantic -O3")
  message("Release mode")

else()

  set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic -O0 -g")
  message("Debug mode")

endif()

# Save the command line compile commands in the build output
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)
# set the binary output path to ../bin/
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../bin/lib)

# set include paths
set(MISSION_COMPUTER_INCLUDE submodules/mission-computer/src)
set(GLM_INCLUDE submodules/glm)

# create project hackadon-all
project(hackadon-all ${USED_LANGUAGES})

# let the preprocessor know which configuration to build
add_compile_definitions(BUILD_MODE_MAPPING_EVALUATION)

add_subdirectory(src)
